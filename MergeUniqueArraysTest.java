/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sw_hw;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mai
 */
public class MergeUniqueArraysTest {
    @Test
   public void mergeTest(){
   int[] arr1 = {1,3,5,7,9};
   int[] arr6 = {1,1,3,3,5,5,7,7,9,9};
   int[] arr2 = {0,2,4,6,8};
   int[] arr3 = {0,1,2,3,4,5,6,7,8,9};
   int[] arr4 = {};
   int[] arr5 = {4,5,6};
   int [] arr7 = {1,3,4,5,5,6,7,9};
   int [] arr8 = {9,6,7,5,4,1};
   int[] arr9 = {4,5,6,9,6,7,5,4,1};
   Assert.assertArrayEquals(MergeUniqueArrays.mergeArrays(arr1, arr2), arr3);   // 2 diffrent arrays
   Assert.assertArrayEquals(MergeUniqueArrays.mergeArrays(arr4, arr2), arr2);   // one array is empty
   Assert.assertArrayEquals(MergeUniqueArrays.mergeArrays(arr4, arr4), arr4);   // 2 arrays is empty
   Assert.assertArrayEquals(MergeUniqueArrays.mergeArrays(arr1, arr1), arr6);   // same array
   Assert.assertArrayEquals(MergeUniqueArrays.mergeArrays(arr8, arr5), arr9);   // 2 diffrent in diamentions

   
   


    }
   
    
}
