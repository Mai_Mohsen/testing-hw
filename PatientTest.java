/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sw_hw;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mai
 */
public class PatientTest {
    
            static private Patient p = new Patient ("Ahmed", "Ali");
            static private Patient p2 = new Patient ("Alaa","Adel");
    
    @Test
    public void constractourTest(){
        
        Assert.assertTrue(p.getfname().equals("Ahmed"));
        Assert.assertTrue(p.getlname().equals("Ali"));
    }
    
       @Test
    public void getphNumest(){
        
    Assert.assertEquals(p2.getPhNum(), null);
    }

    
    @Test
    public void getSSNTest(){
        
    Assert.assertEquals(p2.getSSN(), null);
    }
       
    @Test
    public void setSSNTest(){
        
    p.setSSN("12345d");
    Assert.assertEquals(p.getSSN(), "12345d");
    }
    
    @Test
    public void sePhNumTest(){
        
    p.setPhNum("0123456789");
    Assert.assertEquals(p.getPhNum(), "0123456789");
    }
    
    @Test
    public void getfnameTest(){

    Assert.assertEquals(p.getfname(), "Ahmed");
    }
    
    @Test
    public void getlnameTest(){
        
    Assert.assertEquals(p.getlname(), "Ali");
    }
    
}
